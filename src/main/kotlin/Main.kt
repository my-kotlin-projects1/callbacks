import java.math.BigInteger
import java.util.*
import kotlin.concurrent.thread
import kotlin.system.measureTimeMillis

fun main() {
    println("Program start: ${Thread.currentThread().name} ")
    magicNumber(object: CallBack{
        override fun onSuccess(value: BigInteger) {
           println("first number $value")
        }

        override fun onFailure(error: Throwable) {
            println("error occured ${error.message}")
        }
    })

    magicNumber(object: CallBack{
        override fun onSuccess(value: BigInteger) {
            println("second number $value")
        }

        override fun onFailure(error: Throwable) {
            println("error occured ${error.message}")
        }
    })
}

fun magicNumber(callBack: CallBack) {
    thread {
        println("Calculation started: ${Thread.currentThread().name}")
        val number: BigInteger
        val time = measureTimeMillis {
            number = BigInteger.probablePrime(4000, Random())
        }
        if (time > 2000) {
            callBack.onFailure(Throwable("Calculation takes too long"))
        } else {
        callBack.onSuccess(number)
        }
    }
}

interface CallBack {
    fun onSuccess(value: BigInteger)
    fun onFailure(error: Throwable)
}